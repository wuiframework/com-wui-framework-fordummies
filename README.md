# com-wui-framework-fordummies v1.0.0

> WUI Framework project focused on samples.

## Requirements

This library does not have any special requirements, but it depends on
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder), so please see the WUI Builder's requirements, before you will
try to build this project.

## Project Build

Don't you want to waste your time with project build? OK! Project build is fully automated. For more information about project build see
the [WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder) documentation.

## Documentation

This project provides automatically generated documentation in [JSDoc](http://usejsdoc.org/) from TypeScript source by running the `docs`
command.

> NOTE: Documentation can be accessible also from {projectRoot}/build/target/docs/index.html file after successful creation.

## History

### v1.0.0
Initial release.

## License

This software is owned or controlled by WUI Framework authors. 
Use of this software is governed by the BSD-3-Clause License distributed with this material.
  
See the `LICENSE.txt` file distributed for more details.

---

Author Jakub Cieslar - B48779, 
Copyright (c) 2017 [NXP](http://nxp.com/)
